# Server Requirement
* PHP >= 7.1.3
* OpenSSL PHP Extension
* PDO PHP Extension
* Mbstring PHP Extension
* Tokenizer PHP Extension
* XML PHP Extension
* Ctype PHP Extension
* JSON PHP Extension

# Installation guide

* Install Dependencies via Composer

```
composer install
```

* Setup config file on `.env` follow by `.env.example`

* Setup Database by run following command

```
php artisan migrate
php artisan db:seed
```

### You also can use sql file `web_page.sql` to dump in to database.

Access web page on `{{your server URL}}/public`
