<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 19 Oct 2018 15:01:51 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class SubscribedEmail
 * 
 * @property int $id
 * @property string $name
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class SubscribedEmail extends Eloquent
{
	protected $fillable = [
		'name'
	];
}
