<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 19 Oct 2018 15:01:51 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Tag
 * 
 * @property int $id
 * @property string $name
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \Illuminate\Database\Eloquent\Collection $blogs
 *
 * @package App\Models
 */
class Tag extends Eloquent
{
	protected $fillable = [
		'name'
	];

	public function blogs()
	{
		return $this->belongsToMany(\App\Models\Blog::class)
					->withTimestamps();
	}
}
