<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 19 Oct 2018 15:01:51 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Blog
 * 
 * @property int $id
 * @property string $title
 * @property string $short_description
 * @property string $long_description
 * @property string $name
 * @property int $user_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\User $user
 * @property \Illuminate\Database\Eloquent\Collection $tags
 * @property \Illuminate\Database\Eloquent\Collection $comments
 *
 * @package App\Models
 */
class Blog extends Eloquent
{
	protected $casts = [
		'user_id' => 'int'
	];

	protected $fillable = [
		'title',
		'short_description',
		'long_description',
		'name',
		'user_id'
	];

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class);
	}

	public function tags()
	{
		return $this->belongsToMany(\App\Models\Tag::class)
					->withTimestamps();
	}

	public function comments()
	{
		return $this->hasMany(\App\Models\Comment::class);
	}
}
