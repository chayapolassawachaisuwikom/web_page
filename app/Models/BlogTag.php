<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 19 Oct 2018 15:01:51 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class BlogTag
 * 
 * @property int $blog_id
 * @property int $tag_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\Blog $blog
 * @property \App\Models\Tag $tag
 *
 * @package App\Models
 */
class BlogTag extends Eloquent
{
	protected $table = 'blog_tag';
	public $incrementing = false;

	protected $casts = [
		'blog_id' => 'int',
		'tag_id' => 'int'
	];

	protected $fillable = [
		'blog_id',
		'tag_id'
	];

	public function blog()
	{
		return $this->belongsTo(\App\Models\Blog::class);
	}

	public function tag()
	{
		return $this->belongsTo(\App\Models\Tag::class);
	}
}
