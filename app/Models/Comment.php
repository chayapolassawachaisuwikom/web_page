<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 19 Oct 2018 15:01:51 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Comment
 * 
 * @property int $id
 * @property string $subject
 * @property string $content
 * @property int $user_id
 * @property int $parent_id
 * @property int $blog_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\Blog $blog
 * @property \App\Models\Comment $comment
 * @property \App\Models\User $user
 * @property \Illuminate\Database\Eloquent\Collection $comments
 *
 * @package App\Models
 */
class Comment extends Eloquent
{
	protected $casts = [
		'user_id' => 'int',
		'parent_id' => 'int',
		'blog_id' => 'int'
	];

	protected $fillable = [
		'subject',
		'content',
		'user_id',
		'parent_id',
		'blog_id'
	];

	public function blog()
	{
		return $this->belongsTo(\App\Models\Blog::class);
	}

	public function comment()
	{
		return $this->belongsTo(\App\Models\Comment::class, 'parent_id');
	}

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class);
	}

	public function comments()
	{
		return $this->hasMany(\App\Models\Comment::class, 'parent_id');
	}
}
