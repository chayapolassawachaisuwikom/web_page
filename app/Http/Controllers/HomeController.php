<?php

namespace App\Http\Controllers;

use App\Models\Blog;
use App\Models\Comment;
use App\Models\Tag;
use App\Models\SubscribedEmail;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class HomeController extends Controller
{
    /**
     * @return Response
     */
    public function index()
    {
        return view('index', []);
    }

    /**
     * @return Response
     */
    public function about()
    {
        return view('about', []);
    }

    /**
     * @param Request $request
     *
     * @return Response
     */
    public function blog(Request $request)
    {

        $tags = Tag::all();
        $tag  = $request->query('tag');

        if ($tag !== null) {
            $blogs = Blog::whereHas(
                'tags',
                function ($query) use ($tag) {
                    $query->where('id', $tag);
                }
            )->get();
        } else {
            $blogs = Blog::all();
        }

        $popBlogs = Blog::with('comments')
                        ->has('comments')
                        ->withCount('comments')
                        ->orderBy('comments_count', 'desc')
                        ->limit(5)
                        ->get();

        $recentBlogs = Blog::orderBy('created_at', 'desc')
                           ->limit(5)
                           ->get();

        return view(
            'blog',
            [
                'blogs'       => $blogs,
                'popBlogs'    => $popBlogs,
                'recentBlogs' => $recentBlogs,
                'tags'        => $tags
            ]
        );
    }

    /**
     * @return Response
     */
    public function contact()
    {
        return view('contact', []);
    }

    /**
     * @param Request $request
     *
     * @return Response
     */
    public function search(Request $request)
    {
        $keyword = $request->query('keys');

        $blogs = Blog::where('title', 'like', "%$keyword%")
                     ->orWhere('long_description', 'like', "%$keyword%")
                     ->orWhere('short_description', 'like', "%$keyword%")
                     ->get();

        return view('search', ['blogs' => $blogs]);
    }

    /**
     * @param Request $request
     *
     * @return Response
     */
    public function blog_detail(Request $request)
    {
        $id = $request->query('id');

        if ($id !== null) {
            $blog = Blog::find($id);
            if ($blog === null) {
                abort(404);
            }

        } else {
            abort(404);
        }
        $tags     = Tag::all();
        $popBlogs = Blog::with('comments')
                        ->has('comments')
                        ->withCount('comments')
                        ->orderBy('comments_count', 'desc')
                        ->limit(5)
                        ->get();

        $recentBlogs = Blog::orderBy('created_at', 'desc')
                           ->limit(5)
                           ->get();

        return view(
            'blog_detail',
            [
                'blog'        => $blog,
                'popBlogs'    => $popBlogs,
                'recentBlogs' => $recentBlogs,
                'tags'        => $tags
            ]
        );
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function subscribe_email(Request $request)
    {
        if ($request->isMethod('post')) {
            $email                 = $request->post('email');
            $subscribedEmail       = new SubscribedEmail();
            $subscribedEmail->name = $email;
            $subscribedEmail->save();
        }

        return redirect()->back();
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function comment(Request $request)
    {
        if ($request->isMethod('post')) {
            $subject          = $request->post('subject');
            $content          = $request->post('comment');
            $id               = $request->post('id');
            $comment          = new Comment();
            $comment->subject = $subject;
            $comment->content = $content;
            $comment->blog_id = $id;
            $comment->save();
        }

        return redirect()->back();
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function sendmail(Request $request)
    {
        if ($request->isMethod('post')) {
            $name    = $request->post('name');
            $mail    = $request->post('mail');
            $subject = $request->post('subject');
            $message = $request->post('message');

            Mail::send('emails.contact', ['name' => $name, 'mail' => $mail,'subject' => $subject, 'message' => $message], function($message) use ($subject)
            {
                $message->to('admin@admin.com', 'Admin')->subject($subject);
            });
        }

        return redirect()->back();
    }
}
