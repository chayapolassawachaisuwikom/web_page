<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'HomeController@index');
Route::get('/about', 'HomeController@about');
Route::get('/blog', 'HomeController@blog');
Route::get('/contact', 'HomeController@contact');
Route::get('/search', 'HomeController@search');
Route::get('/blog-detail', 'HomeController@blog_detail');
Route::post('/subscribe-email', 'HomeController@subscribe_email');
Route::post('/comment', 'HomeController@comment');
Route::post('/contact', 'HomeController@sendmail');
