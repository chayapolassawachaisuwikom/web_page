<?php

use Illuminate\Database\Seeder;
use \App\Models\User;
use \App\Models\Blog;
use \Carbon\Carbon;

class CommentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('comments')->insert(
            [
                [
                    'subject' => 'Comment one',
                    'content' => 'Comment one',
                    'blog_id' => Blog::where('title', 'Post Three')->first()->id,
                    'user_id' => User::where('name', 'admin')->first()->id,
                    'parent_id' => null,
                    'created_at' =>  Carbon::now()->format('Y-m-d H:i:s')
                ],
                [
                    'subject' => 'Comment one',
                    'content' => 'Comment one',
                    'blog_id' => Blog::where('title', 'Post Two')->first()->id,
                    'user_id' => User::where('name', 'admin')->first()->id,
                    'parent_id' => null,
                    'created_at' =>  Carbon::now()->format('Y-m-d H:i:s')
                ],
            ]
        );
    }
}
