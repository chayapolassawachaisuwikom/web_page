<?php

use Illuminate\Database\Seeder;
use \App\Models\User;
use \App\Models\Tag;
use \App\Models\Blog;
use \Carbon\Carbon;

class BlogsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = User::where('name', 'admin')->first();
        DB::table('blogs')->insert(
            [
                [
                    'title' => 'Post Three',
                    'name' => 'Post Three',
                    'short_description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. ',
                    'long_description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                    Lorem ipsum dolor sit amet',
                    'user_id' => $users->id,
                    'created_at' =>  Carbon::now()->format('Y-m-d H:i:s')
                ],
                [
                    'title' => 'Post Two',
                    'name' => 'Post Two',
                    'short_description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. ',
                    'long_description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                    Lorem ipsum dolor sit amet',
                    'user_id' => $users->id,
                    'created_at' =>  Carbon::now()->format('Y-m-d H:i:s')
                ],
                [
                    'title' => 'Post One',
                    'name' => 'Post One',
                    'short_description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. ',
                    'long_description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                    Lorem ipsum dolor sit amet',
                    'user_id' => $users->id,
                    'created_at' =>  Carbon::now()->format('Y-m-d H:i:s')
                ]
            ]
        );
        DB::table('blog_tag')->insert(
            [
                [
                    'blog_id'     => Blog::where('title', 'Post Three')->first()->id,
                    'tag_id' => Tag::where('name', 'Photos')->first()->id
                ],
                [
                    'blog_id'     => Blog::where('title', 'Post Three')->first()->id,
                    'tag_id' => Tag::where('name', 'Design')->first()->id
                ],
                [
                    'blog_id'     => Blog::where('title', 'Post Two')->first()->id,
                    'tag_id' => Tag::where('name', 'News')->first()->id
                ],
                [
                    'blog_id'     => Blog::where('title', 'Post One')->first()->id,
                    'tag_id' => Tag::where('name', 'News')->first()->id
                ]
            ]
        );
    }
}
