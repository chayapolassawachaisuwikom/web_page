@extends("layout")
@section("main-content")
    <main role="main" class="main">
        <section class="page-top">
            <div class="container">
                <h1>About Us</h1>
            </div>
        </section>
        <div id="content" class="content full">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <article role="article" class="landing">
                            <div class="row">
                                <div class="col-md-6 featured-image">
                                    <img alt="Home office" src="img/home-office.jpg">
                                </div>
                                <div class="col-md-6 introduction">
                                    <h3>A little about us</h3>
                                    <h4>Lorem ipsum dolor sit amet, consectetur adipisicing</h4>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna incididunt ut labore aliqua. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna incididunt ut labore aliqua.</p>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna incididunt ut labore aliqua. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna incididunt ut labore aliqua. ut labore aliqua.</p>
                                </div>
                            </div>
                        </article>
                    </div>
                </div>
            </div>
        </div>
        <div class="region region-after-content">
            <div class="container">
                <div class="team block">
                    <div class="view-header">
                        <h2 class="block-title">Meet our awesome team</h2>
                        <hr>
                        <p class="block-subtitle">We bring you an awesomeness of design, creative skills, thoughts, and ideas.</p>
                    </div>
                    <div class="col-md-3 col-sm-6 views-row">
                        <div class="img-round img-grayscale-hover">
                            <a href="#" class="img-link">
              <span class="img-border">
                <img src="img/team-one.jpg" width="250" height="250" alt="Stephen Maturin">
              </span>
                            </a>
                        </div>
                        <h6><span>Stephen Maturin</span></h6>
                        <p>Technical Architect</p>
                        <a class="button--icon" href="https://www.facebook.com"><i class="fa fa-facebook"></i></a>
                        <a class="button--icon" href="https://twitter.com"><i class="fa fa-twitter"></i></a>
                        <a class="button--icon" href="https://plus.google.com"><i class="fa fa-google-plus"></i></a>
                    </div>
                    <div class="col-md-3 col-sm-6 views-row">
                        <div class="img-round img-grayscale-hover">
                            <a href="#" class="img-link">
              <span class="img-border">
                <img src="img/team-two.jpg" width="250" height="250" alt="Phoebe Caulfield">
              </span>
                            </a>
                        </div>
                        <h6><span>Phoebe Caulfield</span></h6>
                        <p>Project Manager</p>
                        <a class="button--icon" href="https://www.facebook.com"><i class="fa fa-facebook"></i></a>
                        <a class="button--icon" href="https://twitter.com"><i class="fa fa-twitter"></i></a>
                        <a class="button--icon" href="https://plus.google.com"><i class="fa fa-google-plus"></i></a>
                    </div>
                    <div class="col-md-3 col-sm-6 views-row">
                        <div class="img-round img-grayscale-hover">
                            <a href="#" class="img-link">
              <span class="img-border">
                <img src="img/team-three.jpg" width="250" height="250" alt="Nick Adams">
              </span>
                            </a>
                        </div>
                        <h6><span>Nick Adams</span></h6>
                        <p>User Experience</p>
                        <a class="button--icon" href="https://www.facebook.com"><i class="fa fa-facebook"></i></a>
                        <a class="button--icon" href="https://twitter.com"><i class="fa fa-twitter"></i></a>
                        <a class="button--icon" href="https://plus.google.com"><i class="fa fa-google-plus"></i></a>
                    </div>
                    <div class="col-md-3 col-sm-6 views-row">
                        <div class="img-round img-grayscale-hover">
                            <a href="#" class="img-link">
              <span class="img-border">
                <img src="img/team-four.jpg" width="250" height="250" alt="Molly Bloom">
              </span>
                            </a>
                        </div>
                        <h6><span>Molly Bloom</span></h6>
                        <p>Graphic Designer</p>
                        <a class="button--icon" href="https://www.facebook.com"><i class="fa fa-facebook"></i></a>
                        <a class="button--icon" href="https://twitter.com"><i class="fa fa-twitter"></i></a>
                        <a class="button--icon" href="https://plus.google.com"><i class="fa fa-google-plus"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection
