@extends("layout")
@section("main-content")
<section class="intro" id="section1" data-speed="5" data-type="background" style="background-position: 50% 0px;">
    <div class="overlay">
        <div class="headline">
            <div class="region region-headline">
                <div class="flexslider">
                    <ul class="slides">
                        <li>
                            <h2>Creative control</h2>
                            <p>Digital agencies have more flexibility in design.</p>
                        </li>
                        <li>
                            <h2>Custom Block Layouts</h2>
                            <p>Endless possibilities to re-use blocks.</p>
                        </li>
                        <li>
                            <h2>Twig Templates</h2>
                            <p>Bringing designers and developers closer together</p>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <a href="#" id="goto-section2" class="arrow-down">Get started</a>
</section>
<main role="main" class="main">
    <div class="layout-content">

        <section id="section2" class="section">
            <div class="container">

                <div class="region region-before-content">

                    <div id="block-ourservices" class="block">
                        <h2 class="block-title">Drupal 8</h2>
                        <hr>
                        <p class="block-subtitle">We bring you an awesomeness of design, creative skills, thoughts, and ideas.</p>
                    </div>

                    <div id="block-ourfeatures" class="block">

                        <div class="row">

                            <div class="col-sm-4">
                                <div class="service">
                                    <div class="service-icon"><i class="fa fa-file-code-o"></i></div>
                                    <h4>Twig Templates</h4>
                                    <p>Donec consectetur metus eget felis commodo, ut iaculis mi dignissim. Maecenas in suscipit libero, in dictum metus</p>
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="service">
                                    <div class="service-icon"><i class="fa fa-server"></i></div>
                                    <h4>Custom Block Layouts</h4>
                                    <p>Donec consectetur metus eget felis commodo, ut iaculis mi dignissim. Maecenas in suscipit libero, in dictum metus</p>
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="service">
                                    <div class="service-icon"><i class="fa fa-mobile"></i></div>
                                    <h4>Fully responsive</h4>
                                    <p>Donec consectetur metus eget felis commodo, ut iaculis mi dignissim. Maecenas in suscipit libero, in dictum metus</p>
                                </div>
                            </div>

                        </div>

                        <div class="row">

                            <div class="col-sm-4">
                                <div class="service">
                                    <div class="service-icon"><i class="fa fa-html5"></i></div>
                                    <h4>Clean modern design</h4>
                                    <p>Donec consectetur metus eget felis commodo, ut iaculis mi dignissim. Maecenas in suscipit libero, in dictum metus</p>
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="service">
                                    <div class="service-icon"><i class="fa fa-drupal"></i></div>
                                    <h4>Views in Core</h4>
                                    <p>Donec consectetur metus eget felis commodo, ut iaculis mi dignissim. Maecenas in suscipit libero, in dictum metus</p>
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="service">
                                    <div class="service-icon"><i class="fa fa-cogs"></i></div>
                                    <h4>Wysiwyg options</h4>
                                    <p>Donec consectetur metus eget felis commodo, ut iaculis mi dignissim. Maecenas in suscipit libero, in dictum metus</p>
                                </div>
                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </section>

    </div>
</main>
@endsection
