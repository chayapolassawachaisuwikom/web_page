@extends("layout")
@section("main-content")
    <main role="main" class="main">
        <section class="page-top">
            <div class="container">
                <h1>Search</h1>
            </div>
        </section>
        <div id="content" class="content full">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">

                        <div class="search-index">
                            <header>
                                <h3>Displaying {{count($blogs)}} - {{count($blogs)}} of {{count($blogs)}}</h3>
                            </header>
                            @foreach ($blogs as $blog)
                            <article role="article">
                                <h2><a href="{{ action('HomeController@blog_detail', array('id' => $blog->id)) }}" rel="bookmark"><span>{{ $blog->title }}</span></a></h2>
                                <p>{{  str_repeat($blog->short_description, 10)  }}</p>
                            </article>
                            @endforeach
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection
