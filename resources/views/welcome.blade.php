<html lang="en" dir="ltr">
<head>
    <meta charset="utf-8">
    <meta name="MobileOptimized" content="width">
    <meta name="HandheldFriendly" content="true">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Home | Octo</title>
    <link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.min.css" media="all">
    <link rel="stylesheet" href="vendor/bootstrap/css/bootstrap-theme.min.css" media="all">
    <link rel="stylesheet" href="vendor/font-awesome/css/font-awesome.min.css" media="all">
    <link rel="stylesheet" href="vendor/flexslider/flexslider.css" media="all">
    <link rel="stylesheet" href="vendor/owl-carousel/owl.carousel.css" media="all">
    <link rel="stylesheet" href="vendor/owl-carousel/owl.theme.css" media="all">
    <link rel="stylesheet" href="css/styles.css" media="all">

    <!-- Web Fonts  -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Shadows+Into+Light" rel="stylesheet" type="text/css">
</head>

<body>
<header class="header" role="banner">
    <div class="header-top">
        <div class="container">
            <div id="block-socialicons" class="block">
                <ul class="social-icons">
                    <li><a class="button--icon" target="_blank" href="https://www.facebook.com" title=""><i class="fa fa-facebook"></i></a></li>
                    <li><a class="button--icon" target="_blank" href="https://www.linkedin.com" title=""><i class="fa fa-linkedin"></i></a></li>
                    <li><a class="button--icon" target="_blank" href="https://twitter.com" title=""><i class="fa fa-twitter"></i></a></li>
                    <li><a class="button--icon" target="_blank" href="https://plus.google.com" title=""><i class="fa fa-google-plus"></i></a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="header-nav container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-main">
                <i class="fa fa-bars"></i>
            </button>

            <h1 class="logo">
                <a href="index.html" title="Home" rel="home" id="logo">
                    <img src="img/logo.png" alt="Home">
                </a>
            </h1>
        </div>
        <div class="navbar-search">
            <div class="search-icons">
                <a class="open-form"><i class="fa fa-search"></i></a>
                <a class="close-form"><i class="fa fa-times"></i></a>
            </div>

            <div class="region region-header-search">
                <div class="search-block-form block" id="block-octo-search" role="search">

                    <form action="/search/node" method="get" id="search-block-form" accept-charset="UTF-8">
                        <div class="form-item form-type-search form-item-keys form-no-label">
                            <input title="Enter the terms you wish to search for." class="search form-search form-control" placeholder="Search" type="search" id="edit-keys" name="keys" value="" size="15" maxlength="128">
                        </div>
                        <div class="form-actions form-wrapper" id="edit-actions">
                            <input type="submit" id="edit-submit" name="" value="Search" class="button form-submit">
                        </div>
                    </form>

                </div>
            </div>

        </div>
        <div class="navbar-main navbar-collapse collapse">
            <ul class="nav nav-pills nav-main">
                <li><a href="index.html" class="is-active">Home</a></li>
                <li><a href="about-us.html">About Us</a></li>
                <li><a href="blog.html">Blog</a></li>
                <li><a href="contact.html">Contact Us</a></li>
            </ul>
        </div>
    </div>
</header>
@yield('main-content')
<footer id="footer" role="contentinfo">
    <div class="container main-footer">
        <div class="row">
            <div class="col-md-4">
                <h2>Newsletter</h2>
                <p>Keep up on our always evolving product features and technology. Enter your e-mail and subscribe to our newsletter.</p>
                <form id="newsletterForm" action="" method="POST" novalidate="novalidate">
                    <div class="input-group">
                        <input class="form-control" placeholder="Email Address" name="newsletterEmail" id="newsletterEmail" type="text">
                        <span class="input-group-btn"><button class="btn btn-default" type="submit">Go!</button></span>
                    </div>
                </form>
            </div>
            <div class="col-md-4">
                <h2>About Us</h2>
                <p>Nulla nunc dui, tristique in semper vel, congue sed ligula. Nam dolor ligula, faucibus id sodales in, auctor fringilla libero. Nulla nunc dui, tristique in semper vel. Nam dolor ligula, faucibus id sodales in, auctor fringilla libero.</p>
            </div>
            <div class="col-md-4">
                <h2>Contact Us</h2>
                <ul class="contact">
                    <li><i class="fa fa-map-marker"></i><strong>Address:</strong> 1234 Street Name, City Name, Singapore</li>
                    <li><i class="fa fa-phone"></i> <strong>Phone:</strong> (123) 456-7890</li>
                    <li><i class="fa fa-envelope"></i> <strong>Email:</strong><a href="mailto:mail@example.com">mail@example.com</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="footer-copyright">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <p>All Rights Reserved © Octo | Designed &amp; Developed By Chaz Chumley</p>
                </div>
                <div class="col-md-4">
                    <ul class="social-icons">
                        <li><a class="button--icon" target="_blank" href="https://www.facebook.com" title=""><i class="fa fa-facebook"></i></a></li>
                        <li><a class="button--icon" target="_blank" href="https://www.linkedin.com" title=""><i class="fa fa-linkedin"></i></a></li>
                        <li><a class="button--icon" target="_blank" href="https://twitter.com" title=""><i class="fa fa-twitter"></i></a></li>
                        <li><a class="button--icon" target="_blank" href="https://plus.google.com" title=""><i class="fa fa-google-plus"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>

<script src="vendor/jquery/jquery.min.js"></script>
<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<script src="vendor/flexslider/jquery.flexslider-min.js"></script>
<script src="vendor/owl-carousel/owl.carousel.min.js"></script>
<script src="vendor/jquery-scrollTo/jquery.scrollTo.min.js"></script>
<script src="http://maps.google.com/maps/api/js?sensor=true"></script>
<script src="vendor/jquery-gmap/jquery.gmap.min.js"></script>
<script src="js/octo.js"></script>

</body>
</html>


<!DOCTYPE html>
<html lang="en">

@section('htmlheader')
    @include('la.layouts.partials.htmlheader')
@show
<body class="{{ LAConfigs::getByKey('skin') }} {{ LAConfigs::getByKey('layout') }} @if(LAConfigs::getByKey('layout') == 'sidebar-mini') sidebar-collapse @endif" bsurl="{{ url('') }}" adminRoute="{{ config('laraadmin.adminRoute') }}">
<div class="wrapper">

@include('la.layouts.partials.mainheader')

@if(LAConfigs::getByKey('layout') != 'layout-top-nav')
    @include('la.layouts.partials.sidebar')
@endif

<!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper" style="overflow: auto;">
        @if(LAConfigs::getByKey('layout') == 'layout-top-nav') <div class="container"> @endif
        @if(!isset($no_header))
            @include('la.layouts.partials.contentheader')
        @endif

        <!-- Main content -->
            <section class="content {{ $no_padding or '' }}">
                <!-- Your Page Content Here -->
                @yield('main-content')
            </section><!-- /.content -->

            @if(LAConfigs::getByKey('layout') == 'layout-top-nav') </div> @endif
    </div><!-- /.content-wrapper -->

    @include('la.layouts.partials.controlsidebar')

    @include('la.layouts.partials.footer')

</div><!-- ./wrapper -->

@include('la.layouts.partials.file_manager')

@section('scripts')
    @include('la.layouts.partials.scripts')
@show

</body>
</html>
