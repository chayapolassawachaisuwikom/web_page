@extends("layout")
@section("main-content")
    <main role="main" class="main">
        <section class="page-top">
            <div class="container">
                <h1>Blog Detail</h1>
            </div>
        </section>
        <div id="content" class="content full">
            <div class="container">
                <div class="row">
                    <div class="col-md-9">
                        <article class="post" role="article">
                            <div class="single-post-image post-image">
                                <div class="img-thumbnail">
                                    <img src="img/post-image-one.jpg" alt="Post One Image">
                                </div>
                            </div>
                            <div class="post-date">
                                <span class="day">{{ $blog->created_at->format('d') }}</span>
                                <span class="month">{{ $blog->created_at->format('M') }}</span>
                            </div>
                            <div class="post-content">
                                <h2><a href="#">{{ $blog->title }}</a></h2>
                                <div class="post-meta">
                                    <span class="post-meta-user"><i
                                                class="fa fa-user"></i> By {{ $blog->user->name }}</span>
                                    <span class="post-meta-tag">
                                        <i class="fa fa-tag"></i>
                                        @foreach ($blog->tags as $tag)
                                            {{ $tag->name }}@if (!$loop->last),@endif
                                        @endforeach
                                    </span>
                                    <span class="post-meta-comments"><i class="fa fa-comments"></i> <a href="#comments">{{ count($blog->comments) }}
                                            Comments</a></span>
                                </div>
                                <div class="post-content">
                                    {{ str_repeat($blog->long_description, 20) }}
                                </div>
                                <div class="post-share">
                                    <h3><i class="fa fa-share"></i> Share this post</h3>
                                    <img src="img/add-this.png">
                                </div>
                            </div>
                            <section id="comments" class="post-block post-comments">
                                <h3><i class="fa fa-comments"></i>Comments</h3>
                                <article>
                                    @foreach ($blog->comments as $comment)
                                        <div class="comment">
                                            <div class="img-thumbnail">
                                                <div class="user-picture">
                                                    <a href="#">
                                                        <img src="img/chaz_bw.jpg" width="84" height="85"
                                                             alt="Profile picture">
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="comment-block">
                                                <div class="comment-arrow"></div>
                                                <div class="comment-by">
                                                    <strong><span><a title="View user profile" href="#">
                                                            @if(isset($comment->user))
                                                                    {{ $comment->user->name }}
                                                                @else
                                                                    {{ 'Anonymous' }}
                                                                @endif
                                                        </a></span></strong>
                                                    <span class="pull-right">
									    </span>
                                                </div>
                                                <div class="comment-content">
                                                    <p>{{ $comment->content }}</p>
                                                </div>
                                                <div class="comment-date">
                                                    <span class="date pull-right">{{ $comment->created_at->format('D, m/d/Y - h:ia') }}</span>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </article>
                                <div class="post-block post-leave-comment">
                                    <h3>Leave a comment</h3>
                                    <form method="POST" action="{{ action('HomeController@comment') }}"
                                          class="comment-form" id="comment-form">
                                        @csrf
                                        <input type="hidden" name="id" value="{{ $blog->id }}"/>
                                        <div class="form-item">
                                            <label for="subject">Subject</label>
                                            <input class="form-text" type="text" id="subject" name="subject" value=""
                                                   size="60" maxlength="64" placeholder="" required>
                                        </div>
                                        <div class="form-item">
                                            <label for="comment" class="form-required">Comment</label>
                                            <textarea class="form-textarea required" id="comment" name="comment"
                                                      rows="5" cols="60" placeholder="" required></textarea>
                                        </div>
                                        <div class="form-actions">
                                            <input type="submit" id="edit-submit" name="op" value="Save"
                                                   class="button button--primary form-submit">
                                        </div>
                                    </form>
                                </div>
                            </section>
                        </article>
                    </div>
                    <aside class="layout-sidebar-second" role="complementary">
                        <div class="col-md-3">
                            <div class="block">
                                <h4>Categories</h4>
                                <ul class="nav nav-list primary pull-bottom">
                                    @foreach ($tags as $tag)
                                        <li><a href="{{ action('HomeController@blog', array('tag' => $tag->id)) }}">{{ $tag->name  }}</a></li>
                                    @endforeach
                                </ul>
                            </div>
                            <div class="block">
                                <div class="tabs">
                                    <ul class="nav nav-tabs">
                                        <li class="active"><a href="#popularPosts" data-toggle="tab"><i
                                                        class="fa fa-star"></i> Popular</a></li>
                                        <li><a href="#recentPosts" data-toggle="tab">Recent</a></li>
                                    </ul>
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="popularPosts">
                                            <ul class="simple-post-list">
                                                @foreach ($popBlogs as $blog)
                                                    <li>
                                                        <div class="post-image">
                                                            <div class="img-thumbnail">
                                                                <a href="{{ action('HomeController@blog_detail', array('id' => $blog->id)) }}">
                                                                    <img src="img/post-thumb-two.jpg" width="50" height="50" alt="{{ $blog->title }} Thumbnail">
                                                                </a>
                                                            </div>
                                                        </div>
                                                        <div class="post-info">
                                                            <a href="{{ action('HomeController@blog_detail', array('id' => $blog->id)) }}" class="tabbed-title"><span>{{ $blog->title }}</span></a>
                                                            <div class="post-meta">{{ $blog->created_at->format('M d, Y') }}</div>
                                                        </div>
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                        <div class="tab-pane" id="recentPosts">
                                            <ul class="simple-post-list">
                                                @foreach ($popBlogs as $blog)
                                                    <li class="views-row">
                                                        <div class="post-image">
                                                            <div class="img-thumbnail">
                                                                <a href="{{ action('HomeController@blog_detail', array('id' => $blog->id)) }}">
                                                                    <img src="img/post-thumb-one.jpg" width="50" height="50" alt="{{ $blog->title }} Thumbnail">
                                                                </a>
                                                            </div>
                                                        </div>
                                                        <div class="post-info">
                                                            <a href="{{ action('HomeController@blog_detail', array('id' => $blog->id)) }}" class="tabbed-title"><span>{{ $blog->title }}</span></a>
                                                            <div class="post-meta">{{ $blog->created_at->format('M d, Y') }}</div>
                                                        </div>
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="block ">
                                <h4>About Us</h4>
                                <p>Nulla nunc dui, tristique in semper vel, congue sed ligula. Nam dolor ligula,
                                    faucibus id sodales in, auctor fringilla libero. Nulla nunc dui, tristique in semper
                                    vel. Nam dolor ligula, faucibus id sodales in, auctor fringilla libero.</p>
                            </div>
                        </div>
                    </aside>
                </div>
            </div>
        </div>
    </main>
@endsection
