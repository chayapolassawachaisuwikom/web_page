@extends("layout")
@section("main-content")
    <main role="main" class="main">
        <section class="page-top">
            <div class="container">
                <h1>Contact Us</h1>
            </div>
        </section>

        <div class="block map">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3988.7994122072296!2d103.83959911456371!3d1.294904762110707!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31da199920e1166b%3A0x7e1137e00b234359!2sAleph+Labs+-+Singapore!5e0!3m2!1sen!2ssg!4v1538718568188" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
        </div>

        <div class="block container contact-intro">
            <h3>Get in touch with us</h3>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam imperdiet ante in metus lobortis, vitae sagittis lorem viverra. In felis leo, posuere a lorem in, vestibulum hendrerit lectus. Vestibulum eget sapien dignissim, vestibulum lectus vitae, pretium mi.</p>
        </div>

        <div id="content" class="content full">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">

                        <form action="{{ action('HomeController@sendmail') }}" class="contact-form">
                            @csrf
                            <div class="form-item">
                                <label for="edit-name" class="form-required">Your name</label>
                                <input type="text" id="edit-name" name="name" value="" size="60" maxlength="255" class="form-text required" required="required" aria-required="true">
                            </div>

                            <div class="form-item">
                                <label for="mail" class="form-required">Your email address</label>
                                <input type="email" id="mail" name="mail" value="" size="60" maxlength="254" class="form-email required" required="required" aria-required="true">
                            </div>

                            <div class="form-item">
                                <label for="subject" class="form-required">Subject</label>
                                <input class="form-text required" type="text" id="subject" name="subject" value="" size="60" maxlength="100" placeholder="" required="required" aria-required="true">
                            </div>

                            <div class="form-item">
                                <label for="message" class="form-required">Message</label>
                                <textarea class="text-full form-textarea required" id="message" name="message" rows="12" cols="60" placeholder="" required="required" aria-required="true"></textarea>
                            </div>
                            <div class="form-actions">
                                <input type="submit" id="submit" name="op" value="Send message" class="button button--primary form-submit">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection
