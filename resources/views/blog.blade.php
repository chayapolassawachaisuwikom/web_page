@extends("layout")
@section("main-content")
    <main role="main" class="main">
        <section class="page-top">
            <div class="container">
                <h1>Blog</h1>
            </div>
        </section>
        <div id="content" class="content full">
            <div class="container">
                <div class="row">
                    <div class="col-md-9">
                        @foreach ($blogs as $blog)
                        <article role="article" class="post post--listing">
                            <div class="single-post-image post-image">
                                <div class="img-thumbnail">
                                    <img src="img/post-image-two.jpg" width="1280" height="500" alt="Post Three Image One">
                                </div>
                            </div>
                            <div class="post-date">
                                <span class="day">{{ $blog->created_at->format('d') }}</span>
                                <span class="month">{{ $blog->created_at->format('M') }}</span>
                            </div>
                            <div class="post-content">
                                <h2><a href="{{ action('HomeController@blog_detail', array('id' => $blog->id)) }}" rel="bookmark"><span>{{ $blog->title }}</span></a></h2>
                                <div class="post-content">
                                    <p>{{ str_repeat($blog->short_description, 10) }}</p>
                                </div>
                                <div class="post-meta">
                                    <span class="post-meta-user"><i class="fa fa-user"></i> By <span><span>{{ $blog->user->name }}</span></span></span>
                                    <span class="post-meta-tag"><i class="fa fa-tag"></i>
									<ul>
                                        @foreach ($blog->tags as $tag)
										<li><a href="{{ action('HomeController@blog', array('tag' => $tag->id)) }}">{{ $tag->name  }}</a></li>
                                        @endforeach
									</ul>
								</span>
                                    <span class="post-meta-comments"><i class="fa fa-comments"></i> <a href="{{ action('HomeController@blog_detail', array('id' => $blog->id)) }}#comments">{{ count($blog->comments) }} Comments</a></span>
                                    <a href="{{ action('HomeController@blog_detail', array('id' => $blog->id)) }}" class="button button--primary button--xs pull-right">Read more...</a>
                                </div>
                            </div>
                        </article>
                        @endforeach
                    </div>
                    <aside class="layout-sidebar-second" role="complementary">
                        <div class="col-md-3">
                            <div class="block">
                                <h4>Categories</h4>
                                <ul class="nav nav-list primary pull-bottom">
                                    @foreach ($tags as $tag)
                                        <li><a href="{{ action('HomeController@blog', array('tag' => $tag->id)) }}">{{ $tag->name  }}</a></li>
                                    @endforeach
                                </ul>
                            </div>
                            <div class="block">
                                <div class="tabs">
                                    <ul class="nav nav-tabs">
                                        <li class="active"><a href="#popularPosts" data-toggle="tab"><i class="fa fa-star"></i> Popular</a></li>
                                        <li><a href="#recentPosts" data-toggle="tab">Recent</a></li>
                                    </ul>
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="popularPosts">
                                            <ul class="simple-post-list">
                                                @foreach ($popBlogs as $blog)
                                                <li>
                                                    <div class="post-image">
                                                        <div class="img-thumbnail">
                                                            <a href="{{ action('HomeController@blog_detail', array('id' => $blog->id)) }}">
                                                                <img src="img/post-thumb-two.jpg" width="50" height="50" alt="{{ $blog->title }} Thumbnail">
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class="post-info">
                                                        <a href="{{ action('HomeController@blog_detail', array('id' => $blog->id)) }}" class="tabbed-title"><span>{{ $blog->title }}</span></a>
                                                        <div class="post-meta">{{ $blog->created_at->format('M d, Y') }}</div>
                                                    </div>
                                                </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                        <div class="tab-pane" id="recentPosts">
                                            <ul class="simple-post-list">
                                                @foreach ($popBlogs as $blog)
                                                <li class="views-row">
                                                    <div class="post-image">
                                                        <div class="img-thumbnail">
                                                            <a href="{{ action('HomeController@blog_detail', array('id' => $blog->id)) }}">
                                                                <img src="img/post-thumb-one.jpg" width="50" height="50" alt="{{ $blog->title }} Thumbnail">
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class="post-info">
                                                        <a href="{{ action('HomeController@blog_detail', array('id' => $blog->id)) }}" class="tabbed-title"><span>{{ $blog->title }}</span></a>
                                                        <div class="post-meta">{{ $blog->created_at->format('M d, Y') }}</div>
                                                    </div>
                                                </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="block ">
                                <h4>About Us</h4>
                                <p>Nulla nunc dui, tristique in semper vel, congue sed ligula. Nam dolor ligula, faucibus id sodales in, auctor fringilla libero. Nulla nunc dui, tristique in semper vel. Nam dolor ligula, faucibus id sodales in, auctor fringilla libero.</p>
                            </div>
                        </div>
                    </aside>
                </div>
            </div>
        </div>
    </main>
@endsection
